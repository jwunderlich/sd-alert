#!/bin/bash
#───────────────────────────────────────────────────────────────────────────────
#   Project name:			SD-Alert
#-------------------------------------------------------------------------------
#   Language                Linux Shell Script
#	Main File				sd-alert.sh
#	Version					1.1 2021020101
#	Project start date		2021-01-21	
#   Keywords                alert,alarm,sd,card,shell,script 
#	Created By				Janos Wunderlich
#   Repository				https://gitlab.com/jwunderlich
#	License					GPL v3
#   Header Version			20210125
#───────────────────────────────────────────────────────────────────────────────	
#   Summary:
#
#	SD-Alert produces Warnings (Tone and Alert Message) if Removable Storage(s) 
#   are not removed after backup.
#───────────────────────────────────────────────────────────────────────────────	
#	Description:
#	
#	sd-alert.sh generates speaker alert tone and notify message on desktop if an 
#	SD-Card or other storage with one of the predefined '.id' files (semaphore
#	file in the root of the Mounted Partition) connected.
#   The point of this idea is to prevent forgetting the storage plugged, 
#	for example after completed backups.
#   The alarms are generated in a random time range between 40 and 90 seconds 
#	to avoid monotony and are short sound sequences to not disturb anyone.
#   The .id file is searched for and recognized on all mounted drives.
#	So you can mount an external storage medium on different systems, 
#	the medium is always recognized as the same.
#───────────────────────────────────────────────────────────────────────────────
#	Instructions:
#
#	* Copy the shell script 'sd-alert.sh' and the config file 'sd-alert.cfg' 
#     to the ~/bin directory as root and make 'sd-alert.sh' executable. 
# 	* Create on each removable storage in the root of the drive a 
#	  semaphore file (for example with the extension *.id).
#	* Define the Variables ID0..IDx in the config file (sd-alert.cfg) according 
#	  to your requirements.
#	* Run the script in the Background (invisible terminal window) at system start 
#	  with 'sd-alert.sh &' or from running Terminal session with 'nohup sd-alert.sh &'.
#	
#	On systems without grapical desktop environment or for testing purposes 
#	you can run the script in a visible terminal window for watching echo outputs with
# 	'sd-alert.sh' without any options. 
#───────────────────────────────────────────────────────────────────────────────
#   Comments:
#
#	reasons for using of semaphore files (.id) to detect connected removable media  
# 	instead of hardware detection (of mounted devices):
#	* More Usability:
#     Can be easier used because don't need any hardware access with sudo privileges.
#	  The .id files can be used on different Machines with different Mountpoints.
#	  (The same storage media can be mounted with different Mountpoints in different
#	  environments as pc/laptop/server) 
#	* More Flexibility:
#	  with changing the storage medium it is not necessary to detect new hardware id
#     or other medium properties, the script can be automated easier.
#   * More Reliability:
#     in Virtual Machines (VMs) works the script more steady, there are no hardware 
#     detection problems
#
#	The script recognizes the environment in which it is running (vm/desktop/laptop)
#	and emits different sounds depending on the environment 
#     
#───────────────────────────────────────────────────────────────────────────────
#	System Requirements:
#
#	* installed ffmpeg package
#	Tested on Bash Version: 5.0.17(1)-release (x86_64-pc-linux-gnu)
#───────────────────────────────────────────────────────────────────────────────
#	Limitations:
#	
#   no detection if the storage medium is dismounted. It is necessary to remove
#	any storage medium immediately after dismounting it.
#───────────────────────────────────────────────────────────────────────────────
#	Known issues:
#	-
#───────────────────────────────────────────────────────────────────────────────
#   List of references:
#
#	https://askubuntu.com/questions/88091/how-to-run-a-shell-script-in-background
#	https://stackoverflow.com/questions/1921279/how-to-get-a-variable-value-if-variable-name-is-stored-as-string
#   https://unix.stackexchange.com/questions/82112/stereo-tone-generator-for-linux
#	https://www.systutorials.com/docs/linux/man/1-speaker-test/
#	https://unix.stackexchange.com/questions/1974/how-do-i-make-my-pc-speaker-beep
#	https://stackoverflow.com/questions/2556190/random-number-from-a-range-in-a-bash-script
#	https://misc.flogisoft.com/bash/tip_colors_and_formatting
#	https://ostechnix.com/check-linux-system-physical-virtual-machine/
#
#───────────────────────────────────────────────────────────────────────────────
#

#Define the Configuration File of Drive id-s
SETTINGS_FILE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/sd-alert.cfg"
#echo "$SETTINGS_FILE"
if [ -f "$SETTINGS_FILE" ];then
    source "$SETTINGS_FILE"
    else
    echo "ERROR !!!"
    echo "Drives Configuraton File $SETTINGS_FILE does not Exists!"
    echo "...script halted."
    echo ""
    exit 1
fi

while true #endless loop
do

	echo ────────────────────────────────────────────────────────────
	echo ----- Checking Time: `date +%F_%T` -------
	echo ────────────────────────────────────────────────────────────

 #create array of Linux Mountpoints, accept only Mountpoints in /media and /mnt
 df_array=()
 while IFS= read -r line; do
     df_array+=( "$line" )
 done < <( df --output=target|grep -E 'mnt|media' ) 

 echo "Mountpoints: ${df_array[@]}"
 
  idpresent=0 # initialize Storage .id present variable
	
  for mountpoint in "${df_array[@]}" #search .id-s in all Mountpaths
  do	
	# echo "Processing Mountpoint: ${mountpoint}"
	x=0 # initialize loop index for STORx Variables
	driveidvarname="ID$x" # initialize driveidvarname variable
	driveidvarcontent="${!driveidvarname}" # initialize driveidvarcontent variable
	#check for driveidvarcontent is not NULL, enable max. 100 Drive id-s.
	while [[ ! -z "$driveidvarcontent" && x -le 100 ]]
	do
	 driveidvarname="ID$x"
	 driveidvarcontent="${!driveidvarname}"
	 driveidfullpath="${mountpoint}/"$driveidvarcontent	 
	 #echo "...$driveidfullpath..."
	 if [ -f "$driveidfullpath" ]; then
	  idpresent=1 
	  #let the line blink in Yellow:
	  echo -e "Drive id found in\033[93;5m\e[1m $mountpoint: $driveidvarcontent\033[0m"
	  #echo "Drive id found in $mountpoint: $driveidvarcontent"
	  break
	 fi
	 ((x++))
	done #check for driveidvarcontent is not NULL
  
  done #search .id-s in all Mountpaths

  if [[ $idpresent -eq 1 ]]; then #Drive for Alerting detected
	  
	  #hostnamectl |grep -E 'Chassis: desktop'||'Chassis: laptop'
	  #hostnamectl |grep -E 'Chassis: vm'
	  Environment=$(hostnamectl |grep 'Chassis:') #Check environment with hostnamectl
	  #echo "$Environment"
	  if [[ $Environment == *"desktop"* ]]; then 
		# Running on Host (desktop):
	    echo " -> ffplay (on Desktop): sine-frequency=1300/duration=0.005"
	    ffplay -f lavfi -i "sine=frequency=1300:duration=0.005" -autoexit -nodisp -loglevel quiet
	  elif [[ $Environment == *"laptop"* ]]; then 
		# Running on Host (laptop):
	    echo " -> ffplay (on Laptop): sine-frequency=1300/duration=0.005"
	    ffplay -f lavfi -i "sine=frequency=1300:duration=0.005" -autoexit -nodisp -loglevel quiet  
	  elif [[ $Environment == *"vm"* ]]; then #check if running in VM
	   # Running in VM:
	   echo " -> ffplay (in VM): sine-frequency=500/duration=0.2"
	   ffplay -f lavfi -i "sine=frequency=500:duration=0.2" -autoexit -nodisp -loglevel quiet
	  else # Running in unknown environment
	   echo " -> ffplay ($Environment): sine-frequency=1300/duration=0.005"
	   ffplay -f lavfi -i "sine=frequency=1300:duration=0.005" -autoexit -nodisp -loglevel quiet
	  fi
	  
	  # Show Alert on Desktop
	  notify-send 'Backup Finished?' 'do the next steps!'
	  idpresent=0
	  else
	  echo No Drives found for Backup.
	fi

	# generate random number between 30-90
	rnd=$(shuf -i 40-90 -n 1)
	echo sleep for $rnd seconds...
	sleep $rnd

 unset df_array #deleting the Mountpoint-List Array

done #endless loop
